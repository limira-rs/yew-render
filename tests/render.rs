//! Tests in `/tests/` to see a user point of view to error messages.
#![feature(trace_macros)]
extern crate yew;
#[macro_use]
extern crate yew_render;

use yew::prelude::*;
use yew::virtual_dom::VNode;

type Cnode = VNode<Comp>;

struct Comp;

#[derive(PartialEq, Clone)]
struct Props {
    with: u32,
    other: u32,
}

enum Msg {
    NoCrash,
    Crashing,
}

impl Default for Props {
    fn default() -> Self {
        Props {
            with: 0,
            other: 0,
        }
    }
}

impl Component for Comp {
    type Message = Msg;
    type Properties = Props;

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Comp
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        unimplemented!();
    }
}

impl Renderable<Comp> for Comp {
    fn view(&self) -> Html<Self> {
        //trace_macros!(true);
        let rs = render! {
            div {}
        };
        //trace_macros!(false);
        rs
    }
}

#[test]
fn nothing_to_do() {
    // Never call render! like this:
    // let _: Cnode = render! { }; // Uncomment this will cause error
}

#[test]
fn misspelled_tag() {
    let _: Cnode = render! {
        div { "Wrong html tag name will cause error at compile-time" }
        //multiplication {} // Uncomment this will cause error
    };
}

#[test]
fn empty_tag() {
    //trace_macros!(true);
    let _: Cnode = render! {
        div {}
    };
    //trace_macros!(true);
    let _: Cnode = render! {
        div ()
    };
}

#[test]
fn empty_tag_at_second_level() {
    //trace_macros!(true);
    let one: Cnode = render! {
        div {
            span ()
        }
    };
    //trace_macros!(false);
    let two: Cnode = render! {
        div {
            div {}
        }
        span {}
    };
    let three: Cnode = render! {
        div {
            div ()
        }
        div {
            span ()
            div {}
        }
        p {}
    };
    match one {
        VNode::VTag(tag) => assert_eq!( tag.tag(), "div"),
        _ => panic!("It is just the wrong type")
    }
    match two {
        VNode::VList(list) => assert_eq!( list.childs.len(), 2),
        _ => panic!("It is just the wrong type")
    }
    match three {
        VNode::VList(list) => assert_eq!( list.childs.len(), 3),
        _ => panic!("It is just the wrong type")
    }
}

#[test]
fn attributes_only_tag() {
    //trace_macros!(true);
    let _: Cnode = render! {
        input (type="checkbox", value="attribute_only_tag",)
    };
    //trace_macros!(false);
}

#[test]
fn simple_inner_tag() {
    let _: Cnode = render! {
        div { "simple_inner_tag" }
    };
}

#[test]
fn for_expression() {
    let vec = vec!["first", "second", "third"];
    let options = vec.iter().map(|item| {
        render! {
            option (value=item){item}
        }
    });
    let _: Cnode = render! {
        select { 
            option (value="None") {"No selection"}
            for options
        }
    };
}

#[test]
fn multi_tags_at_root() {
    //trace_macros!(true);
    let fragment: Cnode = render! {
        select { 
            option (value="None") {"No selection"}
            option (value="First") {"First choice"}
            option (value="2nd") {"Second choice"}
        }
        "How about text directly at root of a fragment?";
        div {}
        span {}
        p {}
    };
    //trace_macros!(false);
    match fragment {
        VNode::VList(list) => assert_eq!( list.childs.len(), 5),
        _ => panic!("It is just the wrong type")
    }
}

#[test]
fn component() {
    //trace_macros!(true);
    let _: Cnode = render! {
        div {}
        Comp:
        div {}
    };
    //trace_macros!(false);
}

#[test]
fn component_with_props() {
    //trace_macros!(true);
    let _: Cnode = render! {
        Comp: (with=1)
        Comp: (other=1)
    };
    //trace_macros!(false);
    let props = Props {with:0, other:1};
    let _: Cnode = render! {
        Comp: (with props.clone())
    };
    //trace_macros!(true);
    let _: Cnode = render! {
        Comp: (with props.clone(), with=4)
    };
    //trace_macros!(false);
    let _: Cnode = render! {
        Comp: (with props, other=5,)
    };
}

fn assert_classes(vtag: &Cnode, classes: &[&str]) {
    let vtag = if let VNode::VTag(vtag) = vtag {
        vtag
    }else{
        panic!("Wrong type");
    };
    classes.iter().for_each(|class| assert!(vtag.classes.contains(&class.to_string())) );
}

#[test]
fn single_class() {
    let class = ["class1"];

    let tag: Cnode = render! {
        div (class="class1")
    };
    assert_classes(&tag, &class);

    let tag: Cnode = render! {
        div (classes="class1")
    };
    assert_classes(&tag, &class);
}

#[test]
fn multi_classes() {
    let class = ["class1", "class2"];

    let tag: Cnode = render! {
        div (class="class1 class2")
    };
    assert_classes(&tag, &class);

    let tag: Cnode = render! {
        div (class="class1" "class2")
    };
    assert_classes(&tag, &class);

    let tag: Cnode = render! {
        div (classes="class1" "class2")
    };
    assert_classes(&tag, &class);
}

#[test]
fn aria_data() {
    //trace_macros!(true);
    let tag: Cnode = render! {
        div (aria-data-aka="some-aria-data-aka") {}
    };
    //trace_macros!(false);

    if let VNode::VTag(tag) = tag {
        assert!(tag.attributes.contains_key("aria-data-aka"));
        assert_eq!(
            tag.attributes.get("aria-data-aka"),
            Some(&"some-aria-data-aka".into())
        );
    } else {
        panic!("vtag expected");
    }
}

#[test]
fn selected_value() {
    let selected_value: Option<String> = Some("val_1".to_string());
    let se: Cnode = render! {
        select (value=selected_value.clone()){
            option (value="val_1"){"Value 1"}
        }
    };

    if let VNode::VTag(se) = se {
        assert_eq!(se.value, selected_value);
    } else {
        panic!("vtag expected");
    }

    let selected_value = None;
    let se: Cnode = render! {
        select (value=selected_value.clone()){
            option (value="val_1"){"Value 1"}
        }
    };

    if let VNode::VTag(se) = se {
        assert_eq!(se.value, None);
    } else {
        panic!("vtag expected");
    }
}

#[test]
fn selected_index() {
    let selected_index = Some(1_usize);
    let se: Cnode = render! {
        select (index=selected_index){
            option (value="val_1"){"Value 1"}
            option (value="val_2"){"Value 2"}
        }
    };

    if let VNode::VTag(se) = se {
        assert_eq!(se.selected_index, selected_index);
    } else {
        panic!("vtag expected");
    }

    let selected_index = None;
    let se: Cnode = render! {
        select (index=selected_index){
            option (value="val_1"){"Value 1"}
            option (value="val_2"){"Value 2"}
        }
    };

    if let VNode::VTag(se) = se {
        assert_eq!(se.selected_index, None);
    } else {
        panic!("vtag expected");
    }
}

#[test]
fn input_value() {
    let tag: Cnode = render! {
        input (value="something")
    };
    if let VNode::VTag(tag) = tag {
        assert_eq!(tag.value, Some("something".to_string()));
    } else {
        panic!("vtag expected");
    }
}

#[test]
fn option_attribute_value() {
    let tag: Cnode = render! {
        option (value="something"){"Something here"}
    };
    if let VNode::VTag(tag) = tag {
        assert!(tag.attributes.contains_key("value"));
        assert_eq!(
            tag.attributes.get("value"),
            Some(&"something".to_string())
        );
    }else{
        panic!("vtag expected");
    }
}

#[test]
fn events() {
    let first: Cnode = render! {
        div (
            id="div_id",
            class="one-class",
            classes="a" "b" "c",
            value="somevalue",
            type="checkbox",
            //index=Some(2),
            checked=true,
            disabled=true,
            href="http://example.com/url",
            onclick=|_| Msg::Crashing,
            ondoubleclick=|_| Msg::Crashing,
            onkeypress=|_| Msg::Crashing,
            onkeydown=|_| Msg::Crashing,
            onkeyup=|_| Msg::Crashing,
            onmousedown=|_| Msg::Crashing,
            onmousemove=|_| Msg::Crashing,
            onmouseout=|_| Msg::Crashing,
            onmouseover=|_| Msg::Crashing,
            onmouseup=|_| Msg::Crashing,
            onblur=|_| Msg::Crashing,
            oninput=|e| {
                // Some complex closure here
                let value = e.value.clone();
                println!("The value is {}", value);
                if value.trim() == "" {
                    return Msg::NoCrash;
                }
                match e.value.as_ref() {
                    "crashing" => Msg::Crashing,
                    _ => Msg::NoCrash,
                }
            },
            onchange=|cd| {
                // Another complex closure
                match cd {
                    ChangeData::Select(select) => {
                        println!("The value is {:?}", select.value());
                        match select.selected_index() {
                            None => Msg::Crashing,
                            Some(_) => Msg::NoCrash,
                        }
                    },
                    ChangeData::Value(value) => {
                        println!("The value is {}", value);
                        match value.as_ref() {
                            "crashing" => Msg::Crashing,
                            _ => Msg::NoCrash,
                        }
                    },
                }
            },
            data-more="more-data-here",
            aria-attr1="first-aria-value",
            aria-attr2="2nd-aria-value",
        ){
            div {}
        }
    };
    if let VNode::VTag(tag) = first{
        assert!(tag.classes.contains("a"));
        assert!(tag.classes.contains("b"));
        assert!(tag.classes.contains("c"));
        assert!(tag.classes.contains("one-class"));
    }else{
        panic!("Why not a vtag?");
    }
}

#[test]
fn complex(){
    let aval = "abc";
    //trace_macros!(true);
    let _: Cnode = render! {
        div (id="div_id", class="a b c", value=aval, onchange=|_| Msg::Crashing, onclick=|_| {
            let _dosomething = "It's weird here?";
            Msg::NoCrash
        }) {
            span (class="a b c", onclick=|_| Msg::NoCrash) { "Span content" }
            div {
                input (type="checkbox", value="what is this")
                span { "complex: div / div1 / span" }
                "complex: div / div: expression in middle";
                span { "complex: div / div1 / span2" }
                "complex: div / div: last expression"
            }
            div {
                "complex: div / div: first expression";
                span { "complex: div / div2 / span: after string" }
            }
        }
    };
    //trace_macros!(false);
}

#[test]
fn multi_index_value_select() {
    // All following lines will cause error
    //let _: Cnode = render! { select (index=Some(1), index=Some(2)) };
    //let _: Cnode = render! { select (index=Some(1), value=Some("2".to_string())) };
    //let _: Cnode = render! { select (value=Some("1".to_string()), index=Some(2)) };
    //let _: Cnode = render! { select (value=Some("1".to_string()), value=Some("2".to_string())) };
}

#[test]
fn hr_br() {
    let _: Cnode = render!{
        p {}
        "Something here";
        hr;;
        "Something here";
        span {}
        br;;
        "Something here";
        span {}
        div {
            div {}
            p {}
            "Something here";
            hr;;
            "Something here";
            span {}
            br;;
            "Something here";
            span {}
        }
    };
}
