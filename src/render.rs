//! This module implement macro `render!`
//! 
use yew::prelude::*;
use yew::virtual_dom::VNode;

// @addChild: for both comp and tag
// @newComp: new component
// @propStart: first property of a component
// @prop: properties of a component
// @compGoInner: (self explains)
// @compChild: 
// @attr: attribute of a html element
// @newTag: new html element
// @tagGoInner: (self explains)
// @forExpr: 
// @expr
// @done:

#[macro_export]
macro_rules! rendering {
    // My parent is `root`
    ( @addChild, root, $stack:ident, $child:ident ) => {
        $stack.push( $child.into() );
    };
    // $child is a sub of last $stack item
    ( @addChild, $other:ident, $stack:ident, $child:ident ) => {
        $crate::render::add_child(&mut $stack, $child.into());
    };
    //===================================//
    //       rules for components        //
    //===================================//
    // component: no properties, with content
    //      Comp: { childs here }
    ( $level:ident, $stack:ident, $comp:ident: {$($inner:tt)*} $($tail:tt)* ) => {{
        compile_error! ( "Passing childs to a component is currently not supported" );
    }};
    // component: with properties, with content
    //      Comp: ( props here ) { childs here }
    ( $level:ident, $stack:ident, $comp:ident: ($($prop:tt)*) {$($inner:tt)*} $($tail:tt)* ) => {{
        compile_error! ( "Passing childs to a component is currently not supported" );
    }};
    // component: with properties, no content
    //      Comp: ( props here )
    ( $level:ident, $stack:ident, $comp:ident: ($($prop:tt)*) $($tail:tt)* ) => {
        rendering! { @newComp, $level, $stack, $comp, ($($prop)*) {} }
        rendering! { $level, $stack, $($tail)* }
    };
    // default component: no properties, no content
    //      Comp:
    ( $level:ident, $stack:ident, $comp:ident: $($tail:tt)* ) => {{
        rendering! { @newComp, $level, $stack, $comp, () {} }
        rendering! { $level, $stack, $($tail)* }
    }};
    ( @newComp, $level:ident, $stack:ident, $comp:ident, ($($prop:tt)*) {$($inner:tt)*} ) => {{
        #[allow(unused_mut)]
        let mut pair = ::yew::virtual_dom::VComp::lazy::<$comp>();
        rendering! { @propStart, pair, $($prop)*, }
        let (props, mut comp) = pair;
        comp.set_props( props );
        $stack.push( comp.into() );
        rendering! { @compGoInner, $level, $stack, $($inner)* }
    }};
    // It starts with a `with props`
    ( @propStart, $pair:ident, with $props:expr, $($prop:tt)* ) => {
        $pair.0 = $props;
        rendering! { @prop, $pair, $($prop)* }
    };
    // No `with props`
    ( @propStart, $pair:ident, $($prop:tt)* ) => {
        rendering! { @prop, $pair, $($prop)* }
    };
    // properties' field
    ( @prop, $pair:ident, $field:ident=$value:expr, $($prop:tt)* ) => {
        // This code is copied from yew's html!
        // I don't understand what transform means!?!
        ($pair.0).$field = ::yew::virtual_dom::vcomp::Transformer::transform(&mut $pair.1, $value);
        rendering! { @prop, $pair, $($prop)* }
    };
    // Done
    ( @prop, $pair:ident, ) => { };
    ( @prop, $pair:ident, , ) => { };
    // Go from level `root` to level `top`
    ( @compGoInner, root, $stack:ident, $($inner:tt)* ) => {
        rendering! { @compChild, top, $stack, $($inner)* }
    };
    // Go from `top`/`sub` to sub
    ( @compGoInner, $level:ident, $stack:ident, $($inner:tt)* ) => {
        rendering! { @compChild, sub, $stack, $($inner)* }
    };
    // Process childs of a component, possible syntax:
    //  Comp: {
    //      child_id1=ChildComp: () {}
    //      child_id2=div () {}
    //  }
    ( @compChild, $level:ident, $stack:ident, $id:ident=$($inner:tt)* ) => {
        // Do nothing for now, just call the last rule
        rendering! { @compChild, $level, $stack, }
    };
    // Done
    ( @compChild, top, $stack:ident, ) => {
        // Do nothing, it is one of top nodes
    };
    // Done, last item if $stack is a child of its previous
    ( @compChild, sub, $stack:ident, ) => {{
        let child = $stack.pop().unwrap();
        $crate::render::add_child(&mut $stack, child);
    }};
    // =========================================== //
    //        rules for html tag attributes        //
    // =========================================== //
    ( @select, none, index ) => {
        // It is ok if a select only have none, or exactly one of index/value attriubte
    };
    ( @select, none, value ) => {
        // It is ok if a select only have none, or exactly one of index/value attriubte
    };
    ( @select, $prev:ident, $cur:ident ) => {
        compile_error! ("At most one `value` or one `index` is allow for a `<select>`!")
        //check_html_tag { $cur }
    };
    // index=expr, (for select element)
    ( @attr, select ($iov:ident), $vtag:ident, index=$value:expr, $($tail:tt)* ) => {
        rendering! { @select, $iov, index }
        $vtag.set_selected_index($value);
        rendering! { @attr, select (index), $vtag, $($tail)* }
    };
    // index=expr, (for other elements)
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, index=$value:expr, $($tail:tt)* ) => {
        compile_error! ( "`index` is not supported by tags other than `select`. Find involved tag in next error!" );
        check_html_tag! { $tag } // This line will force an error on $tag
    };
    // value=expr, (for select element)
    ( @attr, select ($iov:ident), $vtag:ident, value=$value:expr, $($tail:tt)* ) => {
        rendering! { @select, $iov, value }
        $vtag.set_selected_value($value);
        rendering! { @attr, select (value), $vtag, $($tail)* }
    };
    // value=expr, (for option element)
    ( @attr, option ($iov:ident), $vtag:ident, value=$value:expr, $($tail:tt)* ) => {
        $vtag.add_attribute("value", &$value);
        rendering! { @attr, option ($iov), $vtag, $($tail)* }
    };
    // value=expr, (for other elements)
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, value=$value:expr, $($tail:tt)* ) => {
        $vtag.set_value(&$value);
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    };
    // class="class-1 class-2 class-3",
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, class=$classes:expr, $($tail:tt)* ) => {
        $vtag.set_classes( $classes );
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    };
    // class="class-1" "class-2" "class-3",
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, class=$($class:expr)+, $($tail:tt)* ) => {
        $( $vtag.add_class( $class.as_ref() ); )+
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    };
    // classes="class-1" "class-2" "class-3",
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, classes=$($class:expr)+, $($tail:tt)* ) => {
        $( $vtag.add_class( $class.as_ref() ); )+
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    };
    // type = "sometype", // checkbox...
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, type=$value:expr, $($tail:tt)* ) => {
        $vtag.set_kind(&$value);
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    }; 
    // checked = expr,
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, checked=$value:expr, $($tail:tt)* ) => {
        $vtag.set_checked($value);
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    }; 
    // disabled = expr,
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, disabled=$value:expr, $($tail:tt)* ) => {
        if $value {
            $vtag.add_attribute("disabled", &"true");
        }
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    };
    // href = expr,
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, href=$value:expr, $($tail:tt)* ) => {
        let href: ::yew::html::Href = $value.into();
        $vtag.add_attribute("href", &href);
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    };
    // Events: onaction = |var| expr,
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onclick = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onclick, |$var: ::yew::prelude::ClickEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, ondoubleclick = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, ondoubleclick, |$var: ::yew::prelude::DoubleClickEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onkeypress = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onkeypress, |$var: ::yew::prelude::KeyPressEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onkeydown = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onkeydown, |$var: ::yew::prelude::KeyDownEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onkeyup = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onkeyup, |$var: ::yew::prelude::KeyUpEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onmousedown = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onmousedown, |$var: ::yew::prelude::MouseDownEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onmousemove = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onmousemove, |$var: ::yew::prelude::MouseMoveEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onmouseout = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onmouseout, |$var: ::yew::prelude::MouseOutEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onmouseover = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onmouseover, |$var: ::yew::prelude::MouseOverEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onmouseup = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onmouseup, |$var: ::yew::prelude::MouseUpEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onblur = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onblur, |$var: ::yew::prelude::BlurEvent| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, oninput = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, oninput, |$var: ::yew::prelude::InputData| $handler, $($tail)* }
    };
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, onchange = |$var:pat| $handler:expr, $($tail:tt)* ) => {
        rendering! { @event, $tag ($iov), $vtag, onchange, |$var: ::yew::prelude::ChangeData| $handler, $($tail)* }
    };
    ( @event, $tag:ident ($iov:ident), $vtag:ident, $event:ident, $handler:expr, $($tail:tt)* ) => {{
        let handler = $handler;
        let listener = ::yew::html::$event::Wrapper::from(handler);
        $vtag.add_listener(Box::new(listener));
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    }};
    // other attributes
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, $attr:ident=$value:expr, $($tail:tt)* ) => {
        $vtag.add_attribute( stringify!($attr), &$value );
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    };
    // aria-*, data-*, 
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, $($attr:ident)-+ = $value:expr, $($tail:tt)* ) => {
        let attr = vec![$(stringify!($attr).to_string()),+].join("-");
        $vtag.add_attribute(&attr, &$value);
        rendering! { @attr, $tag ($iov), $vtag, $($tail)* }
    };
    // Done
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, ) => {};
    ( @attr, $tag:ident ($iov:ident), $vtag:ident, , ) => {};
    // ==================================== //
    //        rules html tag content        //
    // ==================================== //
    // A special syntax for br
    //      br;;
    ( $level:ident, $stack:ident, br;; $($tail:tt)* ) => {{
        let br = ::yew::virtual_dom::VTag::new("br");
        rendering! { @addChild, $level, $stack, br }
        rendering! { $level, $stack, $($tail)* }
    }};
    // A special syntax for hr
    //      hr;;
    ( $level:ident, $stack:ident, hr;; $($tail:tt)* ) => {{
        let hr = ::yew::virtual_dom::VTag::new("hr");
        rendering! { @addChild, $level, $stack, hr }
        rendering! { $level, $stack, $($tail)* }
    }};
    // no attribute; with inner content
    //      div { content of div here }
    ( $level:ident, $stack:ident, $tag:ident { $($inner:tt)* } $($tail:tt)* ) => {
        $stack.push( rendering! { @newTag, $tag, }.into() );
        rendering! { @tagGoInner, $level, $stack, $($inner)* }
        rendering! { $level, $stack, $($tail)* }
    };
    // with attributes; with inner content
    //      div ( attributes here ) { content of div here }
    ( $level:ident, $stack:ident, $tag:ident ($($attr:tt)+) { $($inner:tt)* } $($tail:tt)* ) => {
        $stack.push( rendering! { @newTag, $tag, $($attr)+ }.into() );
        rendering! { @tagGoInner, $level, $stack, $($inner)* }
        rendering! { $level, $stack, $($tail)* }
    };
    // with attributes; no inner content, use for a self-closing html tag
    //      input ( attribute=value pairs here )
    ( $level:ident, $stack:ident, $tag:ident ($($attr:tt)*) $($tail:tt)* ) => {{
        let vtag = rendering! { @newTag, $tag, $($attr)* };
        rendering! { @addChild, $level, $stack, vtag }
        rendering! { $level, $stack, $($tail)* }
    }};
    // Create new tag with attributes
    ( @newTag, $tag:ident, $($attr:tt)* ) => {{
        check_html_tag! { @isvalid $tag } // If $tag is not valid, it will cause an error
        #[allow(unused_mut)]
        let mut vtag = ::yew::virtual_dom::VTag::new( stringify!($tag) );
        rendering! { @attr, $tag (none), vtag, $($attr)*, }
        vtag
    }};
    ( @tagGoInner, root, $stack:ident, $($inner:tt)* ) => {
        rendering! { top, $stack, $($inner)* }
    };
    ( @tagGoInner, $level:ident, $stack:ident, $($inner:tt)* ) => {
        rendering! { sub, $stack, $($inner)* }
    };
    // Childs (the content of an outter html tag) of the parent from an iterator.
    //      for expression;
    ( $level:ident, $stack:ident, for $expr:expr; $($tail:tt)* ) => {
        rendering! {@forExpr, $level, $stack, for $expr }
        rendering! { $level, $stack, $($tail)* }
    };
    // Childs (the content of an outter html tag) of the parent from an iterator.
    // This is the last or the only expression.
    //      for expression
    ( $level:ident, $stack:ident, for $expr:expr ) => {
        rendering! {@forExpr, $level, $stack, for $expr }
        rendering! { @done, $level, $stack }
    };
    // expand for expression
    ( @forExpr, $level:ident, $stack:ident, for $expr:expr ) => {{
        let nodes = $expr;
        let mut vlist = ::yew::virtual_dom::VList::new();
        for node in nodes {
            let node = ::yew::virtual_dom::VNode::from(node);
            vlist.add_child(node);
        }
        rendering! { @addChild, $level, $stack, vlist }
    }};
    // One of childs of the parent
    //      expression;
    ( $level:ident, $stack:ident, $expr:expr; $($tail:tt)* ) => {
        rendering! { @expr, $level, $stack, $expr }
        rendering! { $level, $stack, $($tail)* }
    };
    // The last or the only child of the parent
    //      expression
    ( $level:ident, $stack:ident, $expr:expr ) => {
        rendering! { @expr, $level, $stack, $expr }
        rendering! { @done, $level, $stack }
    };
    // Create node from expression
    ( @expr, $level:ident, $stack:ident, $expr:expr ) => {{
        let node = ::yew::virtual_dom::VNode::from($expr);
        rendering! { @addChild, $level, $stack, node }
    }};
    // Done
    ( $level:ident, $stack:ident, ) => {
        rendering! { @done, $level, $stack }
    };
    // Done for parent_level=sub 
    ( @done, sub, $stack:ident ) => {{
        let child = $stack.pop().unwrap();
        $crate::render::add_child(&mut $stack, child);
    }};
    // Done for parent_level=top
    ( @done, top, $stack:ident ) => {
        // Do nothing, last item will remain in stack
    };
    // Done for parent_level=root
    ( @done, root, $stack:ident ) => {
        if $stack.len() == 1 {
            $stack.pop().unwrap()
        } else {
            ::yew::virtual_dom::VList { childs: $stack }.into()
        }
    };
}

#[macro_export]
macro_rules! render {
    () => {
        compile_error! ( "Empty input to `render!` is not allow" );
    };
        // Input of `rendering!` is not allow to be empty.
    ( $($everything:tt)+ ) => {{
        let mut stack  = Vec::new();
        rendering! { root, stack, $($everything)+ }
    }};
}

type Stack<COMP> = Vec<VNode<COMP>>;

#[doc(hidden)]
pub fn add_child<COMP: Component>(stack: &mut Stack<COMP>, vnode: VNode<COMP>) {
    match stack.last_mut() {
        Some(&mut VNode::VTag(ref mut vtag)) => {
            vtag.add_child(vnode);
        }
        Some(&mut VNode::VList(ref mut vlist)) => {
            vlist.add_child(vnode);
        }
        _ => {
            panic!("can't add child to this type of node");
        }
    }
}
