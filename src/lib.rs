#![deny(missing_docs)]
#![feature(trace_macros)] 

//! yew-render provide a macro called `render!` for use with [yew](https://github.com/DenisKolodin/yew).
//! `render!` is an alternative to yew's `html!`. A yew app can use both of them.
//! `html!` is JSX-like, it tries to stay close to HTML. `render!`, in the other end,
//! try to avoid the closing-tags (`</div>`). `render!` syntax is like:
//! ```rust
//! render! {
//!     div (id="some_id", class="border") {
//!         "Text is quoted and end with semi-colon if there is something behind it at the same level";
//!         span (class="bold") { "This text is bolded!" }
//!         "While this text is regular";
//!         span (class="italic blue") { "This text is blue italic" }
//!     }
//!     div { "Multi tag at root is supported" }
//! }

extern crate yew;

#[macro_use]
#[doc(hidden)]
pub mod html_tags;

#[macro_use]
pub mod render;
