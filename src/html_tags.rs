// This file is generated automatically by build.rs from `html.tags`

#[macro_export]
macro_rules! check_html_tag {
// Content sectioning
	( @isvalid address ) => {};
	( @isvalid article ) => {};
	( @isvalid aside ) => {};
	( @isvalid footer ) => {};
	( @isvalid header ) => {};
	( @isvalid h1 ) => {};
	( @isvalid h2 ) => {};
	( @isvalid h3 ) => {};
	( @isvalid h4 ) => {};
	( @isvalid h5 ) => {};
	( @isvalid h6 ) => {};
	( @isvalid hgroup ) => {};
	( @isvalid nav ) => {};
	( @isvalid section ) => {};
// Text content
	( @isvalid blockquote ) => {};
	( @isvalid dd ) => {};
	( @isvalid div ) => {};
	( @isvalid dl ) => {};
	( @isvalid dt ) => {};
	( @isvalid figcaption ) => {};
	( @isvalid figure ) => {};
	( @isvalid hr ) => {};
	( @isvalid li ) => {};
	( @isvalid main ) => {};
	( @isvalid ol ) => {};
	( @isvalid p ) => {};
	( @isvalid pre ) => {};
	( @isvalid ul ) => {};
// Inline text semantics
	( @isvalid a ) => {};
	( @isvalid abbr ) => {};
	( @isvalid b ) => {};
	( @isvalid bdi ) => {};
	( @isvalid bdo ) => {};
	( @isvalid br ) => {};
	( @isvalid cite ) => {};
	( @isvalid code ) => {};
	( @isvalid data ) => {};
	( @isvalid dfn ) => {};
	( @isvalid em ) => {};
	( @isvalid i ) => {};
	( @isvalid kbd ) => {};
	( @isvalid mark ) => {};
	( @isvalid q ) => {};
	( @isvalid rp ) => {};
	( @isvalid rt ) => {};
	( @isvalid rtc ) => {};
	( @isvalid ruby ) => {};
	( @isvalid s ) => {};
	( @isvalid samp ) => {};
	( @isvalid small ) => {};
	( @isvalid span ) => {};
	( @isvalid strong ) => {};
	( @isvalid sub ) => {};
	( @isvalid sup ) => {};
	( @isvalid time ) => {};
	( @isvalid u ) => {};
	( @isvalid var ) => {};
	( @isvalid wbr ) => {};
// Image and multimedia
	( @isvalid area ) => {};
	( @isvalid audio ) => {};
	( @isvalid img ) => {};
	( @isvalid map ) => {};
	( @isvalid track ) => {};
	( @isvalid video ) => {};
// Embedded content
	( @isvalid embed ) => {};
	( @isvalid iframe ) => {};
	( @isvalid object ) => {};
	( @isvalid param ) => {};
	( @isvalid picture ) => {};
	( @isvalid source ) => {};
// Scripting
	( @isvalid canvas ) => {};
	( @isvalid noscript ) => {};
	( @isvalid script ) => {};
// Demarcating edits
	( @isvalid del ) => {};
	( @isvalid ins ) => {};
// Table content
	( @isvalid caption ) => {};
	( @isvalid col ) => {};
	( @isvalid colgroup ) => {};
	( @isvalid table ) => {};
	( @isvalid tbody ) => {};
	( @isvalid td ) => {};
	( @isvalid tfoot ) => {};
	( @isvalid th ) => {};
	( @isvalid thead ) => {};
	( @isvalid tr ) => {};
// Forms
	( @isvalid button ) => {};
	( @isvalid datalist ) => {};
	( @isvalid fieldset ) => {};
	( @isvalid form ) => {};
	( @isvalid input ) => {};
	( @isvalid label ) => {};
	( @isvalid legend ) => {};
	( @isvalid meter ) => {};
	( @isvalid optgroup ) => {};
	( @isvalid option ) => {};
	( @isvalid output ) => {};
	( @isvalid progress ) => {};
	( @isvalid select ) => {};
	( @isvalid textarea ) => {};
// Interactive elements
	( @isvalid details ) => {};
	( @isvalid dialog ) => {};
	( @isvalid menu ) => {};
	( @isvalid menuitem ) => {};
	( @isvalid summary ) => {};
// Web Components
	( @isvalid slot ) => {};
	( @isvalid template ) => {};
    ( @isvalid $other:ident) => {
        compile_error!("Is it a component or an html tag?\n     * Component => please adds ':' after its name like `Comp:`.\n     * Html tag => misspelled? If it is correct, please open an issue for this. Thank you!\n    Find involved tag in next error!");
        check_html_tag! ($other);
    };
    () => {};
}
