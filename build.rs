use std::env;
use std::fs::File;
use std::io::Write;

fn main() {
    println!("cargo:rerun-if-changed=html.tags");

    let src = env::current_dir().unwrap().join("html.tags");
    let src = std::fs::read_to_string(src).expect("Read content of html.tags");

    let dest = env::current_dir().unwrap().join("src/html_tags.rs");
    let mut out = File::create(dest).expect("Create new html_tags.rs");

    let lines = src.lines()
        .filter(|line| !line.starts_with("//!"))
        .map(|line| line.trim().trim_left_matches('<').trim_right_matches('>'))
        .collect();

    writeln!(
        out,
        "// This file is generated automatically by build.rs from `html.tags`"
    ).unwrap();

    // Generate check_html_tag macro
    generate(
        &mut out,
        "\n#[macro_export]\nmacro_rules! check_html_tag {",
        r#"    ( @isvalid $other:ident) => {
        compile_error!("Is it a component or an html tag?\n    * Component => please adds ':' after its name like `Comp:`.\n    * Html tag => misspelled? If it is correct, please open an issue for this. Thank you!\n    Find involved tag in next error!");
        check_html_tag! ($other);
    };
    () => {};
}"#,
        //"\n}",
        &lines,
        generate_check_html_tag_macro,
    );
}

#[allow(dead_code)]
fn generate_check_html_tag_macro(out: &mut File, line: &str) {
    writeln!(
        out,
        "\t( @isvalid {0} ) => {{}};",
        line
    ).unwrap();
}

fn generate(
    out: &mut File,
    start: &str,
    end: &str,
    lines: &Vec<&str>,
    generator: fn(&mut std::fs::File, &str),
) {
    writeln!(out, "{}", start).unwrap();
    lines.iter().for_each(|line| {
        if line.starts_with("//") {
            writeln!(out, "{}", line).unwrap();
        } else {
            generator(out, line);
        }
    });
    writeln!(out, "{}", end).unwrap();
}
